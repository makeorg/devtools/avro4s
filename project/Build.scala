import sbt.Keys._
import sbt.{Developer, ScmInfo, _}

/** Adds common settings automatically to all subprojects */
object Build extends AutoPlugin {

  object autoImport {
    val org = "org.make"
    val AvroVersion = "1.8.2"
    val Log4jVersion = "1.2.17"
    val ScalatestVersion = "3.1.0"
    val Slf4jVersion = "1.7.29"
    val Json4sVersion = "3.6.7"
    val CatsVersion = "2.0.0-RC2"
    val ShapelessVersion = "2.3.3"
    val RefinedVersion = "0.9.10"
    val MagnoliaVersion = "0.17.0"
  }

  import autoImport._

  override def trigger = allRequirements
  override def projectSettings = publishingSettings ++ Seq(
    organization := org,
    scalaVersion := "2.13.1",
    Test / parallelExecution := false,
    scalacOptions := Seq(
      "-unchecked", "-deprecation",
      "-encoding",
      "utf8",
   //   "-Xfatal-warnings",
      "-feature",
      "-language:higherKinds",
   //   "-Xlog-implicits",
      "-language:existentials"
    ),
    javacOptions := Seq("-source", "1.8", "-target", "1.8"),
    libraryDependencies ++= Seq(
      "org.scala-lang"    % "scala-reflect"     % scalaVersion.value,
      "org.scala-lang"    % "scala-compiler"    % scalaVersion.value,
      "org.apache.avro"   % "avro"              % AvroVersion,
      "org.slf4j"         % "slf4j-api"         % Slf4jVersion          % "test",
      "log4j"             % "log4j"             % Log4jVersion          % "test",
      "org.slf4j"         % "log4j-over-slf4j"  % Slf4jVersion          % "test",
      "org.scalatest"     %% "scalatest"        % ScalatestVersion      % "test"
    )
  )

  val publishingSettings = Seq(
    publishMavenStyle := true,
    Test / publishArtifact  := false,
      credentials += Credentials(
        "Sonatype Nexus Repository Manager",
        "oss.sonatype.org",
        sys.env.getOrElse("OSSRH_USERNAME", ""),
        sys.env.getOrElse("OSSRH_PASSWORD", "")
      ),
    licenses := Seq("MIT" -> new URL("https://opensource.org/licenses/MIT")),
    scmInfo := Some(
      ScmInfo(
        browseUrl = url("https://gitlab.com/makeorg/devtools/avro4s"),
        connection = "scm:git:git://gitlab.com:makeorg/devtools/avro4s.git",
        devConnection = Some("scm:git:ssh://gitlab.com:makeorg/devtools/avro4s.git")
      )
    ),
    developers := List(
      Developer(
        id = "flaroche",
        name = "François LAROCHE",
        email = "fl@make.org",
        url = url("https://github.com/larochef")
      ),
      Developer(
        id = "cpestoury",
        name = "Charley PESTOURY",
        email = "cp@make.org",
        url = url("https://gitlab.com/cpestoury")
      ),
      Developer(
        id = "csalmon-legagneur",
        name = "Colin SALMON-LEGAGNEUR",
        email = "salmonl.colin@gmail.com",
        url = url("https://gitlab.com/csalmon-")
      ),
      Developer(id = "pda", name = "Philippe de ARAUJO", email = "pa@make.org", url = url("https://gitlab.com/philippe.da"))
    ),
    organizationHomepage := Some(url("https://make.org")),
    homepage := Some(url("https://gitlab.com/makeorg/devtools/avro4s"))
  )
}
