resolvers += Classpaths.sbtPluginReleases

addSbtPlugin("org.scalameta"  % "sbt-scalafmt"           % "2.4.2")
addSbtPlugin("org.scalastyle" %% "scalastyle-sbt-plugin" % "1.0.0")
addSbtPlugin("com.geirsson"   % "sbt-ci-release"         % "1.5.6")